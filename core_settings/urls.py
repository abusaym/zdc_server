from django.contrib import admin
from django.urls import path

from Server.views import receive_message

urlpatterns = [
    path("admin/", admin.site.urls),
    path("recieve/", receive_message, name='receive_message'),
]
