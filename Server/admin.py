from django.contrib import admin
from Server.models import Messages


@admin.register(Messages)
class MessagesAdmin(admin.ModelAdmin):
    list_display = ('id', 'message', 'created_at')
    search_fields = ('id', 'message')
    readonly_fields = ('id', 'created_at')

    def has_add_permission(self, request):
        return False
