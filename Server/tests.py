from django.test import TestCase
from django.core.exceptions import ValidationError
from .models import Messages
import uuid

import datetime


class MessagesModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Messages.objects.create(message='Test message')

    def test_message_content(self):
        message = Messages.objects.first()
        expected_object_name = str(message.id)
        self.assertEquals(expected_object_name, str(message.id))
        self.assertEquals(message.message, 'Test message')

    def test_clean_method(self):
        message = Messages(message='   ')  # empty message
        with self.assertRaises(ValidationError):
            message.full_clean()

        message = Messages(message='x' * 300)  # long message
        with self.assertRaises(ValidationError):
            message.full_clean()

        message = Messages(message='Test message')
        message.full_clean()  # should not raise ValidationError

    def test_id_is_uuid(self):
        message = Messages.objects.first()
        self.assertIsInstance(message.id, uuid.UUID)

    def test_created_at_is_auto_added(self):
        message = Messages.objects.create(message='Test message 2')
        self.assertIsInstance(message.created_at, datetime.datetime)

    def test_str_method_returns_message(self):
        message = Messages.objects.first()
        self.assertEqual(str(message), message.message)

    def test_message_max_length(self):
        message = Messages.objects.first()
        max_length = message._meta.get_field('message').max_length
        self.assertEquals(max_length, 255)

    def test_message_ordering(self):
        Messages.objects.create(message='Test message 2')
        Messages.objects.create(message='Test message 3')
        messages = Messages.objects.all()
        self.assertEqual(messages[0].message, 'Test message')
        self.assertEqual(messages[1].message, 'Test message 2')
        self.assertEqual(messages[2].message, 'Test message 3')
