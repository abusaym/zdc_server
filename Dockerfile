# Use an official Python runtime as a parent image
FROM python:3.8

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set the working directory to /code
WORKDIR /code

# Copy the requirements file into the container
COPY requirements.txt /code/

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code into the container
COPY . /code/

# Expose the port that the application will run on
EXPOSE 8000

# Run the command to start the Django development server
CMD ["sh", "-c", "python manage.py makemigrations && python manage.py migrate && echo 'from django.contrib.auth.models import User; User.objects.create_superuser(\"admin\", \"admin@test.com\", \"admin2023\")' | python manage.py shell && python manage.py runserver 0.0.0.0:8000"]
