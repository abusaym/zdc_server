from django.core.exceptions import ValidationError
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Messages


@csrf_exempt
def receive_message(request):
    if request.method == 'POST':
        data = request.POST.get('message')
        message = Messages(message=data)
        try:
            message.full_clean()
        except ValidationError as e:
            return JsonResponse({'error': str(e)}, status=400)
        else:
            message.save()
            print(f"{message.created_at}\tmessage:\t{message}")
            return HttpResponse(status=200)
    else:
        return HttpResponse(status=405)
