import uuid

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _


class Messages(models.Model):
    id = models.UUIDField(_('ID'), primary_key=True, default=uuid.uuid4, editable=False)
    message = models.CharField(_('message'), max_length=255, editable=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _("Message")
        verbose_name_plural = _("Messages")

    def __str__(self):
        return self.message

    def clean(self):
        # Check that the message is not empty
        if not self.message.strip():
            raise ValidationError("Message cannot be empty")

        # Check that the message is not too long
        max_length = self._meta.get_field('message').max_length
        if len(self.message) > max_length:
            raise ValidationError(f"Message must be at most {max_length} characters")
